import serial
import RPi.GPIO as GPIO
import time
import subprocess


port= "/dev/ttyACM0"
s1 = serial.Serial(port, 9600)
s1.flushInput()

while True:
    if s1.inWaiting()>0:
        inputValue = s1.read(1)
        print(ord(inputValue))
        if inputValue != 0 :
            subprocess.call('./captureVideo.sh')
        time.sleep(7)
        