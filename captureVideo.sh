#!/bin/sh
#Script that checks if another recording is happening, then runs a python script to start recording on the camera

#checks if another recording is already working
retVal="$(ps -aux | grep 'GoProStream.py' | grep -c -v grep)"
echo $retVal;
if [ $retVal -eq 0 ]
then
    #runs the python script for the camera to record
	python /home/pi/GoProStream-master/GoProStream.py
else 
	echo Another recording is happening
fi